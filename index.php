<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <section>
        <div class="container">
            <div class="col-md-4"></div>
            <div class="col-md-4">

            <h2>Email exist check</h2>
            <div class="alert-danger">
                    <?php  session_start();
                        if(isset($_SESSION['msz'])) {
                            echo $_SESSION['msz'];
                            session_destroy();
                        }
                    ?>
            </div>
            <div class="alert-success">
                    <?php
                        if(isset($_SESSION['success'])) {
                            echo $_SESSION['success'];
                            session_destroy();
                        }
                    ?>
            </div>
                <form action="./store.php" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" id="" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" class="form-control" id="" placeholder="Email">
                        </div>
                    </div>
                    <!-- <div class="col-sm-offset-2 col-sm-10"> -->
                    <button type="submit" name="submit" class="btn btn-success form-control">submit</button>
                    <!-- </div> -->
                </form>

            </div>
            <div class="col-md-4"></div>
        </div>
    </section>



<script src="dist/lib/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>